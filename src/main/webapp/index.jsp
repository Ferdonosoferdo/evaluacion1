<%-- 
    Document   : index
    Created on : 19-09-2021, 23:36:57
    Author     : gmesatica
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <h1>Calcula tu inversión</h1>
        <form action="InversionController" method="POST">
  <div class="form-group">
    <label for="capital">Capital</label>
    <input placeholder="desde 100000 a 10000000" required="" title="Hola"
        type="number" name="capital" class="form-control" id="capital">
  </div>
  <div class="form-group">
    <label for="tasa">Tasa de interés</label>
    <input required="" 
        type="number" name="tasa" class="form-control" id="tasa">
  </div>
    <div class="form-group">
    <label for="anos">Años</label>
    <input required=""
        type="number" name="anos" class="form-control" id="anos">
  </div>
  
  <button type="submit" class="btn btn-default">Submit</button>
</form>
    </body>
</html>
