/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluacion1.model;

/**
 *
 * @author Fermín.Donoso
 */
public class Calculadora {


    private double capital;
    private double tasa;
    private double años;
    private double resultado;
    
    public  Calculadora (){
    
    }
    
   
    
//    
//    /**
//     * @return the capital
//     */
//    public double getCapital() {
//        return capital;
//    }
//
//    /**
//     * @param capital the capital to set
//     */
//    public void setCapital(double capital) {
//        this.capital = capital;
//    }
//
//    /**
//     * @return the tasa
//     */
//    public double getTasa() {
//        return tasa;
//    }
//
//    /**
//     * @param tasa the tasa to set
//     */
//    public void setTasa(double tasa) {
//        this.tasa = tasa;
//    }
//
//    /**
//     * @return the años
//     */
//    public double getAños() {
//        return años;
//    }
//
//    /**
//     * @param años the años to set
//     */
//    public void setAños(double años) {
//        this.años = años;
//    }
//        /**
//     * @return the resultado
//     */
    public double getResultado() {
        return resultado ;
      
    }

    /**
     * @param resultado the resultado to set
     */
    public void setResultado(double resultado) {
        this.resultado = resultado;
       
    }

       public void calcular(double capital,double tasa, double años){
   
//           this.capital = capital;
//           this.tasa = tasa;
//           this.años = años;
//        
           double resultado = (capital *(tasa/100))* años;
           setResultado(resultado);
           
   }
  
//  I = C * (i/100) * n; donde I = interés simple producido, C = capital, i = tasa interés anual y n = número de años 
//Ejemplo: Un capital de $100.000 a una tasa anual del 10% en 3 años genera un interés simple de: 
//I = $100.000 * 0.1 * 3 = $30.000 


 
    
    @Override
    public String toString() {

        return  
              ""+getResultado()
               ;
    }
    
}
