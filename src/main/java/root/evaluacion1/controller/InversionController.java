/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluacion1.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.evaluacion1.model.Calculadora;

/**
 *
 * @author Fermín donoso
 */
@WebServlet(name = "InversionController", urlPatterns = {"/InversionController"})
public class InversionController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet InversionController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet InversionController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String capital = request.getParameter("capital");
        String tasa = request.getParameter("tasa");
        String anos = request.getParameter("anos");
 
        
         System.out.println("Capital"+capital);
         System.out.println("Tasa inversión : %"+tasa);
         System.out.println("años"+anos);
          
         
        double capitalnum = Double.parseDouble(capital);
        double tasanum = Double.parseDouble(tasa);
        double añosnum = Double.parseDouble(anos);

        Calculadora calculadora = new Calculadora();
     
//        calculadora.setCapital(capitalnum);
//        calculadora.setTasa(tasanum);
//        calculadora.setAños(añosnum);
       calculadora.calcular(capitalnum, tasanum, añosnum);
       
       String resultado = calculadora.toString();
      // Double resultado = calculadora.getResultado();
        System.out.println(resultado); 
        
       // Double hala = calculadora.getResultado();
       
//        
//        float resultado = capitalnum * 2;       
//        System.out.println("resultado"+resultado);
        
//        processRequest(request, response);

    
    request.setAttribute("Capital", capital);
    request.setAttribute("Tasa", tasa);
    request.setAttribute("Anos", anos);
    request.setAttribute("Resultado", resultado);
    //AGREGAMOS LO QUE MOSTRAREMOS NUESTRO JSP DE SALIDA
    request.getRequestDispatcher("Resultado.jsp").forward(request, response);
    
    
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
